package com.elgiire.fractal.core.function;

import org.junit.Test;

import static org.junit.Assert.*;
import static com.elgiire.fractal.core.function.Complex.*;
import static java.lang.Math.*;
import java.util.Locale;

public class ComplexTest {

    /**
     * Test of re method, of class Complex.
     */
    @Test
    public void testRe() {
        assertEquals(0, ZERO.re(), DELTA);
        assertEquals(1, ONE.re(), DELTA);
        assertEquals(0, i.re(), DELTA);
    }

    /**
     * Test of im method, of class Complex.
     */
    @Test
    public void testIm() {
        assertEquals(0, ZERO.im(), DELTA);
        assertEquals(0, ONE.im(), DELTA);
        assertEquals(1, i.im(), DELTA);
    }

    /**
     * Test of vectorLength method, of class Complex.
     */
    @Test
    public void testVectorLength() {
        assertEquals(0, ZERO.vectorLength(), DELTA);
        assertEquals(1, ONE.vectorLength(), DELTA);
        assertEquals(1, i.vectorLength(), DELTA);
        assertEquals(25, new Complex(3, 4).vectorLength(), DELTA);
    }

    /**
     * Test of arg method, of class Complex.
     */
    @Test
    public void testArg() {
        assertEquals(0, ONE.arg(), DELTA);
        assertEquals(0, ONE.multiply(100).arg(), DELTA);
        assertEquals(PI / 2, i.arg(), DELTA);
        assertEquals(PI / 2, i.multiply(100).arg(), DELTA);
        assertEquals(PI, ONE.negate().arg(), DELTA);
        assertEquals(PI, ONE.negate().multiply(100).arg(), DELTA);
        assertEquals(-PI / 2, i.negate().arg(), DELTA);
        assertEquals(-PI / 2, i.negate().multiply(100).arg(), DELTA);
    }

    /**
     * Test of modulus method, of class Complex.
     */
    @Test
    public void testModulus() {
        assertEquals(0, ZERO.modulus(), DELTA);
        assertEquals(1, i.modulus(), DELTA);
        assertEquals(5, new Complex(3, 4).modulus(), DELTA);
    }

    /**
     * Test of conjugate method, of class Complex.
     */
    @Test
    public void testConjugate() {
        assertEquals(ZERO, ZERO.conjugate());
        assertEquals(ONE, ONE.conjugate());
        assertEquals(i.negate(), i.conjugate());
        assertEquals(ONE.add(i.negate()), ONE.add(i).conjugate());
    }

    /**
     * Test of negate method, of class Complex.
     */
    @Test
    public void testNegate() {
        assertEquals(ZERO, ZERO.negate());
        assertEquals(new Complex(-1), ONE.negate());
        assertEquals(new Complex(0, -1), i.negate());
        assertEquals(new Complex(-1, -1), ONE.add(i).negate());
    }

    /**
     * Test of inverse method, of class Complex.
     */
    @Test
    public void testInverse() {
        assertEquals(ZERO, ZERO.inverse());
        assertEquals(ONE, ONE.inverse());
        assertEquals(i.inverse(), i.negate());
        assertEquals(new Complex(1, 1).inverse(), new Complex(0.5, -0.5));
    }

    /**
     * Test of add method, of class Complex.
     */
    @Test
    public void testAdd() {
        assertEquals(ZERO, ZERO.add(0));
        assertEquals(ZERO, ZERO.add(ZERO));
        assertEquals(ONE, ZERO.add(1));
        assertEquals(ONE, ZERO.add(ONE));
        assertEquals(new Complex(2), ONE.add(1));
        assertEquals(new Complex(2), ONE.add(ONE));
        assertEquals(new Complex(1, 1), ONE.add(i));
    }

    /**
     * Test of multiply method, of class Complex.
     */
    @Test
    public void testMultiply() {
        assertEquals(ZERO, ZERO.multiply(0));
        assertEquals(ZERO, ZERO.multiply(ZERO));
        assertEquals(ZERO, ZERO.multiply(1));
        assertEquals(ZERO, ZERO.multiply(ONE));
        assertEquals(new Complex(2), ONE.multiply(2));
        assertEquals(new Complex(4), new Complex(2).multiply(2));
        assertEquals(Complex.fromPolarForm(12, PI/3 + PI/4), Complex.fromPolarForm(3, PI/4)
                .multiply(Complex.fromPolarForm(4, PI/3)));
        assertEquals(ONE.negate(), i.multiply(i));
    }

    /**
     * Test of divide method, of class Complex.
     */
    @Test
    public void testDivide() {
        assertEquals(ZERO, ZERO.divide(ZERO));
        assertEquals(ZERO, ZERO.divide(ONE));
        assertEquals(ONE, ONE.divide(ONE));
        assertEquals(new Complex(2), new Complex(2).divide(ONE));
        Complex c = new Complex(1, 2);
        Complex z = new Complex(2, 1);
        assertEquals(z.inverse().multiply(c), c.divide(z));
    }

    /**
     * Test of equals method, of class Complex.
     */
    @Test
    public void testEquals() {
        assertFalse(ZERO.equals(null));
        assertFalse(ZERO.equals(new Object()));

        assertTrue(ZERO.equals(ZERO));
        assertTrue(ONE.equals(1.0));
        assertFalse(ONE.equals(2));
        assertFalse(ONE.add(i).equals(1.0));
    }

    /**
     * Test of hashCode method, of class Complex.
     */
    @Test
    public void testHashCode() {
        int hash = new Double(i.re()).hashCode() + new Double(i.im()).hashCode();
        assertEquals(hash, i.hashCode());

        Complex c = Complex.randComplex();
        hash = new Double(c.re()).hashCode() + new Double(c.im()).hashCode();
        assertEquals(hash, c.hashCode());
    }

    /**
     * Test of toString method, of class Complex.
     */
    @Test
    public void testToString() {
        Locale.setDefault(Locale.ENGLISH);
        assertEquals("0.00 +0.00i", ZERO.toString());
        assertEquals("1.00 +0.00i", ONE.toString());
        assertEquals("-1.00 +0.00i", ONE.negate().toString());
        assertEquals("0.00 +1.00i", i.toString());
        assertEquals("0.00 -1.00i", i.negate().toString());
        assertEquals("1.00 +1.00i", ONE.add(i).toString());
        assertEquals("-1.00 +1.00i", ONE.negate().add(i).toString());
        assertEquals("1.00 -1.00i", ONE.add(i.negate()).toString());
        assertEquals("-1.00 -1.00i", ONE.add(i).negate().toString());
    }

    /**
     * Test of pow method, of class Complex.
     */
    @Test
    public void testPow() {
        assertEquals(ZERO, ZERO.pow(2));
        assertEquals(ONE, ONE.pow(2));
        assertEquals(ONE.negate(), i.pow(2));
        assertEquals(ONE, new Complex(2).pow(0));
        assertEquals(new Complex(2), new Complex(4).pow(0.5));
    }

    /**
     * Test of exp method, of class Complex.
     */
    @Test
    public void testExp() {
        assertEquals(ONE, ZERO.exp());
        assertEquals(new Complex(E), ONE.exp());
        assertEquals(new Complex(pow(E, 5)), new Complex(5).exp());
        assertEquals(i, new Complex(0, PI / 2).exp());
        assertEquals(i.negate(), new Complex(0, -PI / 2).exp());
    }

    /**
     * Test of sin method, of class Complex.
     */
    @Test
    public void testSin() {
        assertEquals(ZERO, ZERO.sin());
        assertEquals(ONE, new Complex(PI/2).sin());
        assertEquals(ZERO, new Complex(PI).sin());
        assertEquals(ONE.negate(), new Complex(PI*3/2).sin());
    }

    /**
     * Test of cos method, of class Complex.
     */
    @Test
    public void testCos() {
        assertEquals(ONE, ZERO.cos());
        assertEquals(ZERO, new Complex(PI/2).cos());
        assertEquals(ONE.negate(), new Complex(PI).cos());
        assertEquals(ZERO, new Complex(PI*3/2).cos());
    }

    /**
     * Test of fromPolarForm method, of class Complex.
     */
    @Test
    public void testFromPolarForm() {
        assertEquals(new Complex(-1, 0), new Complex(cos(PI), sin(PI)));
    }

    /**
     * Test of fromPolarForm method, of class Complex.
     */
    @Test
    public void testEqualsDouble() {
        assertTrue(Complex.equalsDouble(0, 0));
        assertFalse(Complex.equalsDouble(0, 1));
    }

}
