package com.elgiire.fractal.main;

import com.elgiire.fractal.gui.MainWindow;

import javax.swing.SwingUtilities;

public class App {

    public static void main(String[] args) {
        SwingUtilities.invokeLater(MainWindow::new);
    }

}
