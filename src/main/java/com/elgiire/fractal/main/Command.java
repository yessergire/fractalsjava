package com.elgiire.fractal.main;

public interface Command {
	void execute();
}