package com.elgiire.fractal.main;

import java.util.Observer;
import java.util.stream.IntStream;

import com.elgiire.fractal.core.color.Color;
import com.elgiire.fractal.core.color.CirclesAndLines;
import com.elgiire.fractal.core.function.Complex;
import com.elgiire.fractal.core.function.ComplexFunction;
import com.elgiire.fractal.core.function.IteratingFunction;
import com.elgiire.fractal.core.function.MandelbrotSet;

public class GeneratorController {
	private final ComputeState state;

	public GeneratorController() {
		state = new ComputeState(new CirclesAndLines());
		state.setComplexFunction(new MandelbrotSet());
                //state.setRadius(0.005);
                //state.setParameter(new Complex(0.004, 1.941));
	}	

	public Command getZoomInCommand() {
		return () -> {
			ComplexFunction func = state.getComplexFunction();
			if (func instanceof IteratingFunction) {
				IteratingFunction iterFunc = (IteratingFunction) func;
				iterFunc.setMaxIteration(iterFunc.getMaxIteration() + 5);
				
			}
			state.setRadius(state.getRadius() * 0.5);
		};
	}

	public Command getZoomOutCommand() {
		return () -> {
			ComplexFunction func = state.getComplexFunction();
			if (func instanceof IteratingFunction) {
				IteratingFunction iterFunc = (IteratingFunction) func;
				iterFunc.setMaxIteration(iterFunc.getMaxIteration() - 5);
				
			}
			state.setRadius(state.getRadius() * 2);
		};
	}

	public Command getResetCommand() {
		return () -> state.reset();
	}

	public Command getMoveUpCommand() {
		return () -> state.setParameter(state.getParameter().subtract(new Complex(0, state.getRadius())));
	}

	public Command getMoveLeftCommand() {
		return () -> state.setParameter(state.getParameter().subtract(state.getRadius()));
	}

	public Command getMoveRightCommand() {
		return () -> state.setParameter(state.getParameter().add(state.getRadius()));
	}

	public Command getMoveDownCommand() {
		return () -> state.setParameter(state.getParameter().add(new Complex(0, state.getRadius())));
	}

	public IntStream getRGB(int width, int height) {
		return state.getRGB(width, height);
	}

	public ComputeState getState() {
		return state;
	}

	public Command setComplexFunction(ComplexFunction complexFunction) {
		return () ->  state.setComplexFunction(complexFunction);
	}

	public Command setColor(Color color) {
		return () -> state.setColor(color);
	}

	public void addObserver(Observer observer) {
		state.addObserver(observer);
	}
}
