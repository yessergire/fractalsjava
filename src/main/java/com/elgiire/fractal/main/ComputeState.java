package com.elgiire.fractal.main;

import com.elgiire.fractal.core.color.Color;
import com.elgiire.fractal.core.color.ContourLines;
import com.elgiire.fractal.core.function.Complex;
import com.elgiire.fractal.core.function.ComplexFunction;
import com.elgiire.fractal.gui.SimpleAction;

import java.util.Observable;
import java.util.stream.IntStream;

public class ComputeState extends Observable {
    private double radius;
    private Complex parameter;
    private ComplexFunction complexFunction;
    private Color color;
    private ContourLines cl;
    private boolean useLines;
    private boolean unit;
    private int inc;

    public boolean isUnit() {
        return unit;
    }

    public void setUnit(boolean unit) {
        this.unit = unit;
    }

    public ComputeState(Color color) {
        radius = 2;
        parameter = Complex.ZERO;
        this.color = color;
        cl = new ContourLines(color);
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
        update();
    }

    public Complex getParameter() {
        return parameter;
    }

    public void setParameter(Complex parameter) {
        this.parameter = parameter;
        update();
    }

    public ComplexFunction getComplexFunction() {
        return complexFunction;
    }

    public void setComplexFunction(ComplexFunction complexFunction) {
        this.complexFunction = complexFunction;
        update();
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        cl.setPicture(color);
        this.color = color;
        update();
    }

    void reset() {
        radius = 2;
        parameter = Complex.ZERO;
        update();
    }

    private void update() {
        setChanged();
        notifyObservers();
    }

    public IntStream getRGB(int width, int height) {
        return IntStream.range(0, height * width)
                .map((index) -> getRGB(getPoint(index, width, height)));
    }

    public int[][] getRGBInt(int width, int height) {
        double asp = 1.0 * width / height;
        int[][] ints = new int[width][height];
        for(int i =0; i < width; i++)
            for(int j =0; j < height; j++)
                ints[i][j] = getRGB(complexFunction.calculate(new Complex(
                        (parameter.re() - radius * asp) + (2.0f * radius * asp / width) * i,
                        (parameter.im() - radius) + (2.0f * radius / height) * (height - j))));
        return ints;
    }

    private Complex getPoint(int index, int width, int height) {
        double asp = 1.0 * width / height;
        return complexFunction.calculate(new Complex(
                (parameter.re() - radius * asp) + (2.0f * radius * asp / width) * (index % width),
                (parameter.im() - radius) + (2.0f * radius / height) * (height - (index / width))));
    }
    
    private int getRGB(Complex c) {
        if (c.isNaN()) return 0;
        if (c.isInfinite()) return 0x0;
        if (unit && c.modulus() > 1.05) return 0xffffff;
        //if (unit && !(c.arg() > 0 && c.arg() < Math.PI)) return 0xffffff;
        float[] hsb = (useLines)? cl.getColor(c): color.getColor(c);
        return java.awt.Color.HSBtoRGB(hsb[0], hsb[1], hsb[2]);
    }
    
    public boolean isUseLines() {
        return useLines;
    }

    public void setUseLines(boolean useLines) {
        this.useLines = useLines;
        update();
    }

    public void toggleUseLines() {
        useLines = !useLines;
        update();
    }

    public void toggleUnit() {
        unit = !unit;
        update();
    }
}
