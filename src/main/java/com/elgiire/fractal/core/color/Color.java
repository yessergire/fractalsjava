package com.elgiire.fractal.core.color;

import com.elgiire.fractal.core.function.Complex;

/**
 * This interface represents a function which maps a complex to an RGB value.
 */
public interface Color {
    float[] getColor(Complex c);
}