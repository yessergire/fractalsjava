
package com.elgiire.fractal.core.color;

import com.elgiire.fractal.core.function.Complex;
import com.elgiire.fractal.core.function.IteratingFunction;
import com.elgiire.fractal.main.ComputeState;

public class IterationColoring implements Color {
    private static int[] palette = new int[128];
    private final ComputeState state;

    {
        for (int i = 0; i < 128; i++) {
            char r = (char) (((128-i-1) * 2) << 16);
            char g = (char) (((128-i-1) * 2) << 8);
            char b = (char) 0xff;
            palette[i] = r+g+b;
        }
    }

    public IterationColoring(ComputeState state) {
        this.state = state;
    }
    
    public float[] getColor(Complex c) {
        IteratingFunction f = (IteratingFunction) state.getComplexFunction();
        int iter = f.getIteration(c);
        float hue = (float) Math.sin(iter);
        float sat = (float) Math.cos(iter);
        return new float[]{hue, sat, hue*sat};//) + palette[iter%128]) / 2;
    }
}
