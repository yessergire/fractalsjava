package com.elgiire.fractal.core.color;

import com.elgiire.fractal.core.function.Complex;
import static java.lang.Math.PI;
import static java.lang.Math.floor;
import static java.lang.Math.log;

/**
 *
 * @author Yessergire
 */
public class ContourLines implements Color {

    private Color picture;
    public Color getPicture() {
        return picture;
    }

    public void setPicture(Color pic) {
        this.picture = pic;
    }

    public ContourLines(Color pic) {
        this.picture = pic;
    }
    
    @Override
    public float[] getColor(Complex c) {
        float[] hsb = picture.getColor(c);
        float b = (float) log(c.modulus());
        float phase = (float) (log(c.modulus()));
        return new float[] { hsb[0], hsb[1], hsb[2] * sawTooth(b) * sawTooth(phase)};
    }

    private float sawTooth(float x) {
        return (float) (0.5 + 0.5 * (x - floor(x)));
    }
}
