package com.elgiire.fractal.core.color;

import static java.lang.Math.PI;
import static java.lang.Math.floor;
import static java.lang.Math.log;

import java.util.function.BiFunction;

import com.elgiire.fractal.core.function.Complex;

public class PhasePlotWithContourLines implements Color {

	@Override
	public float[] getColor(Complex c) {
        if (c.isNaN()) return new float[]{0f, 0f, 1f};
        if (c.isInfinite()) return new float[]{1f,1f,1f};

        float hue = (float) (c.arg() / (2 * PI) + 1);

        int n = 16; // n=number of isochromatic lines per cycle
        float isol = sawTooth(hue, (float) (1.0 / n)); //# isochromatic lines

        float brightness = (float) log(c.modulus());
        float modc = sawTooth(brightness, (float) (2 * PI / n)); // lines of constant log-modulus

        return new float[]{ hue, 1, modc * isol};
	}
        
        private float sawTooth(float x, float t) {
            x = x / t;
            return (float) (0.7 + 0.3 * (x - floor(x)));
        }
}
