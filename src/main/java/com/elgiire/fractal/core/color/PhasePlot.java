package com.elgiire.fractal.core.color;

import com.elgiire.fractal.core.function.Complex;

/**
 *
 * @author Yessergire
 */
public class PhasePlot implements Color {
    public float[] getColor(Complex c) {
        return new float[]{(float) c.divide(c.modulus()).arg(), 1, 1};
    }
}
