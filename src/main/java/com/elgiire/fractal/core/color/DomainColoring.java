package com.elgiire.fractal.core.color;

import static java.lang.Math.PI;
import static java.lang.Math.pow;

import com.elgiire.fractal.core.function.Complex;

public class DomainColoring implements Color {

	@Override
	public float[] getColor(Complex c) {
        double ranges = 0;
        double rangee = 1;
        double m = c.modulus();
        while (m > rangee) {
            ranges = rangee;
            rangee *= Math.E;
        }

        double k = (m - ranges) / (rangee - ranges);

        double arg = c.arg();
        if (arg < 0)
            arg += PI * 2;
        arg /= PI * 2;

        double sat = (k < 0.5) ? (k * 2) : (1 - (k - 0.5) * 2);
        sat = 1 - pow((1 - sat), 3);
        sat = 0.4 + sat * 0.6;


        double val = (k < 0.5) ? (k * 2) : (1 - (k - 0.5) * 2);
        val = 1 - val;
        val = 1 - pow((1 - val), 3);
        val = 0.6 + val * 0.4;

        return new float[] {(float) arg, (float) sat, (float) val};
	}

}
