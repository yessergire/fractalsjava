/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elgiire.fractal.core.color;

import com.elgiire.fractal.core.function.Complex;
import static java.lang.Math.floor;

/**
 *
 * @author Yessergire
 */
public class ArcsAndRays implements Color {
        int n = 15; // Lines width is 1/n.
        int zoom = n * 5;
        float f = 1f/4;
        
    @Override
    public float[] getColor(Complex c) {
        if (isUnitCircle(c))
            return new float[] {1, (float) 1, (float) 1};
        
        if (isImaginaryLine(c))
            return new float[] {1 * f, (float) 1, (float) 1};
        
        if (isRealLine(c))
            return new float[] {2 * f, (float) 1, (float) 1};
        
        if (isRay(c))
            return new float[] {3 * f, (float) 1, (float) 1};
        
        if (isCircle(c))
            return new float[] {1, (float) 1, (float) 0};

        return new float[] {(float) 1, (float) 0, (float) 1};
    }
    
    private boolean isUnitCircle(Complex c) {
       return (floor(c.modulus()) == 1 && c.modulus() - floor(c.modulus()) < 1f/zoom);
    }
    
    private boolean isCircle(Complex c) {
       return (floor(c.modulus()*zoom) % n == 0);
    }
    
    private boolean isRealLine(Complex c) {
       return (floor(c.re()) == 0 && c.re() - floor(c.re()) < 1f/zoom);
    }
    
    private boolean isImaginaryLine(Complex c) {
       return (floor(c.im()) == 0 && c.im() - floor(c.im()) < 1f/zoom);
    }
    
    private boolean isRay(Complex c) {
        int args = 8;
        for (int index = 0; index <= args; index++)
          if (equals(c.arg(), index*Math.PI/args, 1f/zoom)|| equals(c.arg(), -index*Math.PI/args, 1f/zoom))
              return true;
        return false;
    }
    
    private boolean isReal(Complex c) {
       return (floor(c.re()*zoom) % n == 0);
    }
    
    private boolean isImaginary(Complex c) {
       return (floor(c.im()*zoom) % n == 0);
    }
    
    private boolean equals(double a, double b, double delta) {
        return (Math.abs(a - b) < delta);
    }
    
}
