package com.elgiire.fractal.core.color;

import static java.lang.Math.PI;
import static java.lang.Math.pow;

import com.elgiire.fractal.core.function.Complex;

public class ColorWheel implements Color {

    public float[] getColor(Complex c) {
        float hue = (float) (c.arg() / (2 * PI));
        float brightness = (float) pow(1.0 - 1.0 / (1 + pow(c.modulus(), 2)), 0.2);
        return new float[]{ hue, 1, brightness};
    }
;

}
