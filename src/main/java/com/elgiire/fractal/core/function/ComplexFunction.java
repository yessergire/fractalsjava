package com.elgiire.fractal.core.function;


/**
 * This interface represents a complex function.
 */
public interface ComplexFunction {
    Complex calculate(Complex c);
}
