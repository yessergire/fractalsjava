package com.elgiire.fractal.core.function;

public class JuliaSet extends IteratingFunction {
    private int lastIteration;
    private static Complex c = Complex.i;//Complex.fromPolarForm(1, Math.PI * 3.0/7);

    @Override
    public Complex calculate(Complex z) {
        for (int i = 0; z.modulus() <= (1 + Math.sqrt(1 + 4 * z.modulus())) / 2 && i < maxIteration; i++) {
            z = z.multiply(z).add(c);
            lastIteration = i;
        }
        if (lastIteration > total)
            total = lastIteration;
        return z;
    }

    @Override
    public int getIteration(Complex c) {
        calculate(c);
        return lastIteration;
    }

}
