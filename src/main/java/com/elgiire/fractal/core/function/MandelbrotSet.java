package com.elgiire.fractal.core.function;

public class MandelbrotSet extends IteratingFunction {

    @Override
    public Complex calculate(Complex c) {
        Complex z = Complex.ZERO;
        for (int i = 0; z.modulus() <= 4 && i < maxIteration; i++) {
            z = z.multiply(z).add(c);
        }
        return z;
    }

    @Override
    public int getIteration(Complex c) {
        int i;
        Complex z = Complex.ZERO;
        for (i = 0; z.modulus() <= 4 && i < maxIteration; i++) {
            z = z.multiply(z).add(c);
        }
        return i;
    }

}
