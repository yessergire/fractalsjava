package com.elgiire.fractal.core.function;

public abstract class IteratingFunction implements ComplexFunction {

    protected int maxIteration = 256;
    protected int total;

    public int getMaxIteration() {
        return maxIteration;
    }

    public void setMaxIteration(int maxIteration) {
        this.maxIteration = maxIteration;
    }

    public int getTotal() {
        return total;
    }

    public abstract int getIteration(Complex c);
}
