package com.elgiire.fractal.core.function;

public class Complex {

    public static final double DELTA = 1e-14;
    public final static Complex i = new Complex(0, 1);
    public final static Complex ONE = new Complex(1);
    public final static Complex ZERO = new Complex();
    private double re, im;

    public Complex() {
    }

    public Complex(double re) {
        this(re, 0);
    }

    public Complex(double re, double im) {
        this.re = (re == 0) ? 0 : re;
        this.im = (im == 0) ? 0 : im;
    }

    public static Complex fromPolarForm(double r, double theta) {
        return new Complex(r * Math.cos(theta), r * Math.sin(theta));
    }

    public static Complex randComplex() {
        return new Complex(Math.random(), Math.random());
    }

    protected static boolean equalsDouble(double a, double b) {
        return Math.abs(a - b) <= DELTA;
    }

    public double re() {
        return re;
    }

    public double im() {
        return im;
    }

    public double vectorLength() {
        return re * re + im * im;
    }

    public double arg() {
        return Math.atan2(im, re);
    }

    public double modulus() {
        return Math.sqrt(vectorLength());
    }

    public Complex conjugate() {
        return new Complex(re, -im);
    }

    public Complex negate() {
        return new Complex(-re, -im);
    }

    public Complex inverse() {
        return conjugate().multiply(new Complex(1 / vectorLength()));
    }

    public Complex add(double a) {
        return new Complex(a + re, im);
    }

    public Complex add(Complex c) {
        return new Complex(c.re + re, c.im + im);
    }

    public Complex subtract(Complex c) {
        return new Complex(re - c.re, im - c.im);
    }
    public Complex subtract(double a) {
        return new Complex(re - a, im);
    }

    public Complex multiply(double d) {
        return new Complex(d * re, d * im);
    }

    public Complex multiply(Complex z) {
        double real = z.re * re - z.im * im;
        double imaginary = z.re * im + z.im * re;
        return new Complex(real, imaginary);
    }

    public Complex divide(double d) {
        return multiply(1 / d);
    }

    public Complex divide(Complex c) {
        return multiply(c.inverse());
    }

    @Override
    public boolean equals(Object o) {
        if ((o instanceof Double) && equalsDouble(im, 0)) {
            return equalsDouble(re, (Double) o);
        }

        if ((o instanceof Complex)) {
            return equalsComplex((Complex) o);
        }
        return false;
    }

    private boolean equalsComplex(Complex c) {
        return equalsDouble(c.re, re) && equalsDouble(c.im, im);
    }

    @Override
    public int hashCode() {
        return new Double(re).hashCode() + new Double(im).hashCode();
    }

    @Override
    public String toString() {
        return String.format("%.2f %+.2fi", re, im);
    }

    public Complex pow(double n) {
        double r = Math.pow(modulus(), n);
        double theta = arg() * n;
        return new Complex(r * Math.cos(theta), r * Math.sin(theta));
    }

    public Complex exp() {
        double ex = Math.pow(Math.E, re);
        return fromPolarForm(ex, im);
        //return new Complex(ex * Math.cos(im), ex * Math.sin(im));
    }

    public Complex sin() {
        Complex z1 = multiply(i);
        Complex z2 = multiply(i).negate();
        return z1.exp().add(z2.exp().negate()).divide(i.multiply(2));
    }

    public Complex cos() {
        Complex z1 = multiply(i);
        Complex z2 = multiply(i).negate();
        return z1.exp().add(z2.exp()).divide(2);
    }

    public Complex tan() {
        return sin().divide(cos());
    }

    public boolean isNaN() {
        return Double.isNaN(re) || Double.isNaN(im);
    }

    public boolean isInfinite() {
        return Double.isInfinite(re) || Double.isInfinite(im);
    }

    public Complex log() {
        return new Complex(Math.log(modulus()), arg());
    }

    public Complex  abs() {
        double x = re, y = im;
        if (x < 0) x = -x;
        if (y < 0) y = -y;
        return new Complex(x, y);
    }



}
