
package com.elgiire.fractal.core.function;
public class Ship extends IteratingFunction {

    @Override
    public Complex calculate(Complex c) {
        Complex z = new Complex();
        for (int i = 0; z.modulus() <= 10 && i < maxIteration; i++) {
            z = (z.abs().multiply(z.abs())).add(c);
        }
        return z;
    }

    @Override
    public int getIteration(Complex c) {
        int i;
        Complex z = new Complex();
        for (i = 0; z.modulus() <= 10 && i < maxIteration; i++) {
            z.abs();
            z = (z.multiply(z)).add(c);
        }
        return i;
    }

}
