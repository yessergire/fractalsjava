package com.elgiire.fractal.gui;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import com.elgiire.fractal.main.Command;

@SuppressWarnings("serial")
public class SimpleAction extends AbstractAction {
    private final Command command;

    public SimpleAction(String text, Command command) {
        super(text);
        this.command = command;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
    	command.execute();
    }
}
