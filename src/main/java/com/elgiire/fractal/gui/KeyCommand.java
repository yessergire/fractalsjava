package com.elgiire.fractal.gui;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import com.elgiire.fractal.main.Command;
import com.elgiire.fractal.main.GeneratorController;

/**
 * Listens keyPresses and acts accordingly.
 */
public class KeyCommand implements KeyListener {
    private GeneratorController controller;
    private Command up, left, down, right, zoomIn, zoomOut;

    public KeyCommand(GeneratorController d) {
        controller = d;
        up = controller.getMoveUpCommand();
        left = controller.getMoveLeftCommand();
        down = controller.getMoveDownCommand();
        right = controller.getMoveRightCommand();
        zoomIn = controller.getZoomInCommand();
        zoomOut = controller.getZoomOutCommand();
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_UP: up.execute();
                break;
            case KeyEvent.VK_LEFT: left.execute();
                break;
            case KeyEvent.VK_RIGHT: right.execute();
                break;
            case KeyEvent.VK_DOWN: down.execute();
                break;
            case KeyEvent.VK_SPACE: zoomIn.execute();
                break;
            case KeyEvent.VK_BACK_SPACE: zoomOut.execute();
                break;
        }
    }
    
}
