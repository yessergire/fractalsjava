package com.elgiire.fractal.gui;

import java.awt.image.BufferedImage;

import com.elgiire.fractal.main.GeneratorController;

public final class WindowCreator {

    private int index;
    private final GeneratorController facade;

    public WindowCreator(GeneratorController facade) {
        this.facade = facade;
    }

    public void updateImage(BufferedImage img) {
        int width = img.getWidth();
        int height= img.getHeight();
        index = 0;
        facade.getRGB(width, height)
                .parallel()
                .forEachOrdered((rgb) -> img.setRGB(index % width, index++ / width, rgb));
    }

    public BufferedImage createWindow(int width, int height) {
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        updateImage(image);
        return image;
    }

}
