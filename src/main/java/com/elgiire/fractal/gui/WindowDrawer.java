package com.elgiire.fractal.gui;

import javax.swing.*;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Observable;
import java.util.Observer;

/**
 * Draws, updates, and moves the image.
 */
@SuppressWarnings("serial")
public class WindowDrawer extends JPanel implements Observer {
    private WindowCreator creator;
    private BufferedImage image;

    public WindowDrawer(WindowCreator creator) {
        this.creator = creator;
    }

    @Override
    public void paint(Graphics g) {
        if (image != null) {
            Graphics2D g2 = (Graphics2D) g;
            g.drawImage(image, 0, 0, image.getWidth(), image.getHeight(), null);
        }
        else  {
            image = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_RGB);
            draw();
        }
        super.paintChildren(g);
    }


    public void draw() {
        if (image != null)
            creator.updateImage(image);
        updateUI();
    }

    @Override
    public void update(Observable o, Object arg) {
        draw();
    }

}
