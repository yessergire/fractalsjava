package com.elgiire.fractal.gui;

import java.awt.BorderLayout;
import java.io.IOException;
import java.util.Arrays;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.SpringLayout;

import com.elgiire.fractal.core.color.*;
import com.elgiire.fractal.core.function.*;
import com.elgiire.fractal.main.GeneratorController;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JOptionPane;

@SuppressWarnings("serial")
public class MainWindow extends javax.swing.JFrame {
    private GeneratorController facade;
    private JFileChooser chooser;
    private WindowDrawer drawer;
    private final static ComplexFunction id = (Complex z) -> z;
    private final static ComplexFunction sin = (Complex z) -> z.sin();
    private final static ComplexFunction cos = (Complex z) -> z.cos();
    private final static ComplexFunction tan = (Complex z) -> z.tan();
    private final static ComplexFunction log = (Complex z) -> z.log();
    private final static ComplexFunction exp = (Complex z) -> z.exp();
    private JMenu graphMenu;

    public MainWindow() {
        initComponents();
        setupMenuBar();
        setupControls();
        setVisible(true);
    }

    private void initComponents() {
        facade = new GeneratorController();
        chooser = new JFileChooser();
        drawer = new WindowDrawer(new WindowCreator(facade));
        facade.addObserver(drawer);
        facade.getState().setComplexFunction(id);
        setContentPane(drawer);
        addKeyListener(new KeyCommand(facade));
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
    }

    private void setupMenuBar() {
        JMenuBar jMenuBar = new JMenuBar();
        setJMenuBar(jMenuBar);

        JMenu fileMenu = new JMenu("File");
        fileMenu.add(new JMenuItem(new SimpleAction("Add a function", this::addAction)));
        fileMenu.add(new JMenuItem(new SimpleAction("Export", this::saveAction)));
        fileMenu.add(new JMenuItem(new SimpleAction("Exit", () -> System.exit(0))));
        jMenuBar.add(fileMenu);

        Complex n = new Complex(1, Math.sqrt(3));
        graphMenu = new JMenu("Graph");
        graphMenu.add(new JMenuItem(new SimpleAction("Mandelbrot fractalSet", facade.setComplexFunction(new MandelbrotSet()))));
        graphMenu.add(new JMenuItem(new SimpleAction("Julia fractalSet", facade.setComplexFunction(new JuliaSet()))));
        graphMenu.add(new JMenuItem(new SimpleAction("Ship fractalSet", facade.setComplexFunction(new Ship()))));
        graphMenu.add(new JMenuItem(new SimpleAction("z = z", facade.setComplexFunction(id))));
        graphMenu.add(new JMenuItem(new SimpleAction("z**2", facade.setComplexFunction((z) -> z.multiply(z)))));
        graphMenu.add(new JMenuItem(new SimpleAction("z**2 + 1", facade.setComplexFunction((z) -> z.multiply(z).add(1)))));
        graphMenu.add(new JMenuItem(new SimpleAction("1/(1 + z^2)", facade.setComplexFunction((z) -> z.multiply(z).add(1).inverse()))));
        graphMenu.add(new JMenuItem(new SimpleAction("Sin(z)", facade.setComplexFunction(sin))));
        graphMenu.add(new JMenuItem(new SimpleAction("Cos(z)", facade.setComplexFunction(cos))));
        graphMenu.add(new JMenuItem(new SimpleAction("Tan(z)", facade.setComplexFunction(tan))));
        graphMenu.add(new JMenuItem(new SimpleAction("Log(z)", facade.setComplexFunction(log))));
        graphMenu.add(new JMenuItem(new SimpleAction("Exp(z)", facade.setComplexFunction(exp))));
        graphMenu.add(new JMenuItem(new SimpleAction("z * Exp(Im(z) i)", facade.setComplexFunction((z) -> new Complex(0, z.im()).exp().multiply(z)))));
        jMenuBar.add(graphMenu);

        JMenu coloringCommands = new JMenu("Coloring");
        coloringCommands.add(new JCheckBoxMenuItem(new SimpleAction("Use Contour Lines", () -> facade.getState().toggleUseLines())));
        coloringCommands.add(new JCheckBoxMenuItem(new SimpleAction("Show only unit disk", () -> facade.getState().toggleUnit())));
        coloringCommands.add(new SimpleAction("Arcs and rays", facade.setColor(new ArcsAndRays())));
        coloringCommands.add(new SimpleAction("Circles and lines", facade.setColor(new CirclesAndLines())));
        coloringCommands.add(new SimpleAction("Coloring wheel", facade.setColor(new ColorWheel())));
        coloringCommands.add(new SimpleAction("Domain Coloring", facade.setColor(new DomainColoring())));
        coloringCommands.add(new SimpleAction("Phase Plot", facade.setColor(new PhasePlot())));
        coloringCommands.add(new SimpleAction("Iterations", facade.setColor(new IterationColoring(facade.getState()))));
        jMenuBar.add(coloringCommands);
    }

    private void setupControls() {
        JPanel zoomPanel = new JPanel(new BorderLayout());
        zoomPanel.add(new JButton(new SimpleAction("Zoom in", facade.getZoomInCommand())), BorderLayout.NORTH);
        zoomPanel.add(new JButton(new SimpleAction("Zoom out", facade.getZoomOutCommand())), BorderLayout.SOUTH);
        zoomPanel.add(new JButton(new SimpleAction("Reset", facade.getResetCommand())), BorderLayout.CENTER);
        /*
        zoomPanel.add(new JButton(new SimpleAction("Inc Iter", facade.getIncIterationsCommand())), BorderLayout.EAST);
        zoomPanel.add(new JButton(new SimpleAction("Dec Iter", facade.getDecIterationsCommand())), BorderLayout.WEST);
        */

        JPanel controlPanel = new JPanel(new BorderLayout());
        controlPanel.add(new JButton(new SimpleAction("^", facade.getMoveUpCommand())), BorderLayout.NORTH);
        controlPanel.add(new JButton(new SimpleAction("v", facade.getMoveDownCommand())), BorderLayout.SOUTH);
        controlPanel.add(new JButton(new SimpleAction(">", facade.getMoveRightCommand())), BorderLayout.EAST);
        controlPanel.add(new JButton(new SimpleAction("<", facade.getMoveLeftCommand())), BorderLayout.WEST);
        Arrays.stream(controlPanel.getComponents()).forEach((c) -> c.setFocusable(false));

        SpringLayout layout = new SpringLayout();
        drawer.setLayout(layout);
        layout.putConstraint(SpringLayout.EAST, controlPanel, -10, SpringLayout.EAST, drawer);
        layout.putConstraint(SpringLayout.NORTH, controlPanel, 10, SpringLayout.NORTH, drawer);
        drawer.add(controlPanel);

        layout.putConstraint(SpringLayout.EAST, zoomPanel, -10, SpringLayout.EAST, drawer);
        layout.putConstraint(SpringLayout.NORTH, zoomPanel, 100, SpringLayout.NORTH, drawer);
        Arrays.stream(zoomPanel.getComponents()).forEach((c) -> c.setFocusable(false));
        drawer.add(zoomPanel);
    }

    private void saveAction() {
        if (chooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION)
            new Thread(() -> {
                try {
                    ImageIO.write(new WindowCreator(facade).createWindow(3000, 3000), "png", chooser.getSelectedFile());
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }).start();
    }

    private void addAction() {
        String res = JOptionPane.showInputDialog("Add a function");
        System.out.println(res);
        if (res == null) return;
        
        if (res.equals("z")) {
            graphMenu.add(new JMenuItem(new SimpleAction("Identity f(z) = z", facade.setComplexFunction((z) -> z))));
        }
        else {
            try {
                double f = Double.parseDouble(res);
                graphMenu.add(new JMenuItem(new SimpleAction(String.format("Constant %.2f", f), facade.setComplexFunction((z) -> new Complex(f)))));
            } catch (Exception e) {
            }
        }
    }
}
